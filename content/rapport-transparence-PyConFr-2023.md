Title: Rapport de transparence de la PyConFr 2023
Date: 2023-04-25 21:38:00
Category: news
Slug: rapport-transparence-PyConFr-2023
Author: AFPy

![Logo de la PyConFR 2023 à Bordeaux](https://www.pycon.fr/2023/static/images/banniere.png "Logo de la PyConFR 2023 à Bordeaux")

PyCon France (PyConFR) est une conférence qui a lieu chaque année (sauf circonstances
exceptionnelles) en France. Cette année, elle a eu lieu du
[16 au 19 février à Bordeaux](https://www.pycon.fr/2023/),
rassemblant des personnes de la communauté Python. Les participantes à la conférence
sont tenues de respecter ​le [Code de Conduite](https://www.afpy.org/docs/charte) de
l'Association Francophone Python, l'association qui organise l'événement.

Le but de ce document est d'améliorer l'accueil et la sécurité des participantes ainsi
que de donner aux organisateurs et organisatrices des indicateurs sur le comportement de
la communauté. En effet, pour pouvoir entendre, il faut pouvoir écouter. C'est
maintenant devenu une pratique courante, pour les organisations ayant un Code de
Conduite, de publier un rapport de transparence suite à la tenue d'une conférence.
C'est le but de ce document.

[Télécharger le rapport en PDF.](https://git.afpy.org/AFPy/gestion/src/commit/67cd4d1d6df54dd3d1ac7ce5ac32df6260b885cb/compte_rendus_diversite/2023.pdf)
