Title: Soirée traduction le 27 octobre 2021
Date: 2021-10-22 15:06:10
Category: news
Slug: soiree-traduction-le-27-octobre-2021
Author: AFPy


_TL;DR; Sur BBB le 27 octobre 19h00_

## Atelier Traduction

Qu'est-ce que c'est ?

C'est une soirée, en ligne (cause pandémie), pour s'entre-aider à traduire la documentation de Python en français :

[https://docs.python.org/fr/](https://docs.python.org/fr/)

Une bonne partie est déjà faite, mais une bonne partie reste à faire :

![graphe de la progression de la traduction](https://www.afpy.org/post_image/fr_translation_percent.png "graphe de la progression de la traduction")

### Ça se passe où et quand ?

Sur notre propre BBB :

[https://bbb.afpy.org/b/jul-fss-kpj-txw](https://bbb.afpy.org/b/jul-fss-kpj-txw)

le mercredi 27 octobre 2021 à 19h00 CEST.

### Des questions ou des suggestions

On aimerait reprendre les ateliers en présentiel, si vous voulez proposer un lieu, ou si
vous avez une question venez nous en parler sur [discuss.afpy.org](https://discuss.afpy.org/t/soiree-traduction-du-27-octobre-2021/455).
