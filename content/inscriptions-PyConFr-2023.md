Title: Les inscriptions pour la PyConFr sont ouvertes !
Date: 2022-11-18 11:47:22
Category: news
Slug: inscriptions-PyConFr-2023
Author: AFPy


![Logo de la PyConFR 2023 à Bordeaux](https://www.pycon.fr/2023/static/images/banniere.png "Logo de la PyConFR 2023 à Bordeaux")

Les inscriptions sont ouvertes 🔓 !

Vous pouvez d'ores et déjà indiquer votre présence à la #PyConFR23, qui aura lieu à
Bordeaux du 16 au 19 février 2023. L'inscription est gratuite 🆓 !

[S'inscrire via helloasso](https://www.helloasso.com/associations/afpy/evenements/pyconfr-2023)

Le jeudi et vendredi, place aux Sprints Les développeurs 👨‍💻 et développeuses 👩‍💻 de
différents projets open source se rejoignent pour coder ensemble. Chaque personne est la
bienvenue pour contribuer, et nous cherchons également à accompagner les débutant·e·s 🔰.

Durant le week-end c'est conférences, vous aurez l'occasion de participer à des
présentations sur des sujets variés, autour du langage Python 🐍 , de ses usages, des
bonnes pratiques, des retours d'expériences, des partages d'idées…
