Title: Les adhésions 2022 sont ouvertes et l'AG aura lieu le mercredi 09/02/22 (18h-20h)
Date: 2021-01-29 09:42:07
Category: news
Slug: ouverture-adhesions-2022
Author: AFPy


_Renouvelez ou adhérez à l'AFPy_

Bonjour !

Les [adhésions à l'association](https://www.helloasso.com/associations/afpy/adhesions/adhesion-2022-a-l-afpy)
pour l'année 2022 sont ouvertes !

Cette année encore les adhésions sont gérées à l'aide du site [helloasso](https://www.helloasso.com/).

L'[assemblée générale](https://discuss.afpy.org/t/adhesions-2022-et-assemblee-generale-annuelle/641/5)
aura lieu en [visioconférence](https://bbb.afpy.org/b/jul-cb2-1bd-ost) le mercredi 9 février de 18h à 20h.

Venez visiter notre [forum](https://discuss.afpy.org/) pour prendre connaissance des dernières discussions et y participer !

N'hésitez pas à également consulter les
[rapports et procès verbaux des assemblées précédentes](https://github.com/AFPy/afpy_gestion/tree/master/assemblees_generales/2020).
